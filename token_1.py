import requests
from bs4 import BeautifulSoup
import time
import os
import json
import html

###### Login to Galv ######
# Url of the login webpage
url_login = 'https://auth.galvanize.com/sign_in'

username = os.getenv("GALV_USERNAME")
password = os.getenv("GALV_PASSWORD")

# Creates a session
s = requests.Session()

### Navigating the SSO Page ###

url_SSO = 'https://sis.galvanize.com/accounts/login/'

# Send a GET request to the SSO page
login_page_content = s.get(url_SSO).content

# Parse the HTML content
soup = BeautifulSoup(login_page_content, 'html.parser')

# Find the form with the action '/accounts/login/'
login_form = soup.find('form', attrs={'action': '/accounts/login/'})

# Extract the CSRF token
csrf_token = login_form.find('input', attrs={'name': 'csrfmiddlewaretoken'}).get('value')

# Data for the SSO form
sso_form_data = {
    'csrfmiddlewaretoken': csrf_token
    # Add any other necessary fields
}

# Send a POST request to the SSO form action url
response = s.post(url_SSO, data=sso_form_data)

# Now get the content of the actual login page
login_page_content = s.get(url_login).content

# Parse the HTML content
soup = BeautifulSoup(login_page_content, 'html.parser')

# Find the div with the react props
react_props_div = soup.find('div', attrs={'data-react-class': 'NewSessionForm'})

# Extract the data-react-props value, HTML decode it, and load the JSON
react_props = json.loads(html.unescape(react_props_div['data-react-props']))

# Extract the authenticity_token
authenticity_token = react_props.get('csrfToken')

if authenticity_token is None:
    print("Error: Couldn't find the authenticity_token field")

# Data for the login form
#This data was gathered from the dev tools tab "Network" with "Preserve Log" enabled
login_data = {
    'utf8': '✓',  # This is a common field in forms, you can hard code it
    'authenticity_token': authenticity_token,
    'user[email]': username,
    'user[password]': password,
    'user[remember_me]': '1',  # Assuming you want to be remembered
    # Include any other form fields here
}

# Send a POST request to the login page with the form data
response = s.post(url_login, data=login_data)

# Print out the status code
print("POST request status:", response.status_code)

# # Print out the response text: this should be the HTML of the SIS main page I see when I log in
# print("Response text:", response.text)




###### Token Fill-in ######
# URL of the webpage where the token is located
url_token = 'https://sis.galvanize.com/cohorts/21/attendance/mine/'
while True:
    try:
        # Send a GET request to the webpage
        response = s.get(url_token)

        # Check the response status
        print(f"GET request status: {response.status_code}")

        # Print the HTML content of the page to a file
        with open('token_page.html', 'w') as f:
            f.write(response.text)


        # Create a BeautifulSoup object and specify the parser
        soup = BeautifulSoup(response.text, 'html.parser')

        # Print the entire page to check if it's correct
        # print(f"Page content: {soup.prettify()}")

        # Find the token on the webpage. It's in a <span> tag with class 'tag is-danger is-size-6'
        token_span = soup.find('span', {'class': 'tag is-danger is-size-6'})

        # Find the crsfmiddleware within the specific form. This makes sure we grab the csrf we need as the page does have 2 of them.
        attendance_form = soup.find('form', attrs={'action': '/cohorts/21/attendance/'})
        csrf_token = attendance_form.find('input', attrs={'name': 'csrfmiddlewaretoken'}).get('value')

        # If the token is not found, wait for a while and then continue with the next iteration
        if token_span is None:
            print("Token not found, retrying in 1 second.")
            time.sleep(1)
            continue

        token = token_span.text

        print(f"Found token: {token}")

        # Now we can use this token to POST to the server
        # The website is expecting a form data with 'token' field
        payload = {
            'csrfmiddlewaretoken': csrf_token,
            'token': token,
        }

        # Extract the form action, this ensures robustness if the url gets changed
        form_action = attendance_form['action']

        # Send a POST request with the form data
        post_response = s.post('https://sis.galvanize.com' + form_action, data=payload)


        # Check the response status
        print(f"POST request status: {post_response.status_code}")

        # Check the response
        if post_response.status_code == 200:
            print("Successfully submitted the token!")
            break
        else:
            print("Failed to submit the token. Retrying in 1 second.")
            time.sleep(1)

    except Exception as e:
        print(f"An error occurred: {e}. Retrying in 1 second.")
        time.sleep(1)
